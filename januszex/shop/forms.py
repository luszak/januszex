"""Forms module."""
from django import forms

from shop.models import User, CarBrand, Category, Voivodeship, Post, DeliveryType


VOIVODESHIPS = [(v.id, v.name) for v in Voivodeship.objects.all()]

CAR_BRANDS_CHOICES = [(brand.id, brand.name)
                      for brand in CarBrand.objects.all()]
CAR_BRANDS_CHOICES.insert(0, (0, 'Marka samochodu'))

CATEGORIES_CHOICES = [(category.id, category.name)
                      for category in Category.objects.all()]
CATEGORIES_CHOICES.insert(0, (0, 'Kategoria'))

DELIVERY_TYPES = [
    (delivery.id, delivery.name+" ("+str(delivery.delivery_price)+" zł)")
    for delivery in DeliveryType.objects.all()
]

class UserCreationForm(forms.ModelForm):
    """User creation form model."""
    class Meta:
        model = User
        fields = (
            'username', 'first_name', 'last_name', 'password',
            'email', 'phone_number'
        )
        widgets = {
            'username': forms.TextInput(attrs={'class': 'form-control'}),
            'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            'password': forms.PasswordInput(attrs={'class': 'form-control'}),
            'email': forms.TextInput(attrs={'class': 'form-control'}),
            'phone_number': forms.TextInput(attrs={'class': 'form-control'}),
        }


class UserForm(forms.ModelForm):
    """User form model."""
    class Meta:
        model = User
        fields = (
            'first_name', 'last_name', 'email', 'phone_number'
        )
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.TextInput(attrs={'class': 'form-control'}),
            'phone_number': forms.TextInput(attrs={'class': 'form-control'}),
        }


class AddressForm(forms.Form):
    """Address form."""
    voivodeship = forms.ChoiceField(
        label='Województwo', choices=VOIVODESHIPS,
        widget=forms.Select(choices=VOIVODESHIPS, attrs={'class': 'form-control'})
    )
    city = forms.CharField(
        label='Miejscowość', max_length=20,
        widget=forms.TextInput(attrs={'class': 'form-control'})
    )
    zip_code = forms.CharField(
        label='Kod pocztowy', max_length=6,
        widget=forms.TextInput(attrs={'class': 'form-control'})
    )
    street = forms.CharField(
        label='Ulica', max_length=50, required=False,
        widget=forms.TextInput(attrs={'class': 'form-control'})
    )
    building_number = forms.CharField(
        label='Numer budynku', max_length=8,
        widget=forms.TextInput(attrs={'class': 'form-control'})
    )
    apartment_number = forms.CharField(
        label='Numer mieszkania', max_length=8, required=False,
        widget=forms.TextInput(attrs={'class': 'form-control'})
    )


class PostFindingForm(forms.Form):
    """Finding posts form."""

    title = forms.CharField(
        label='Nazwa', max_length=100, required=False,
        widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Szukana fraza'})
    )
    car_brand = forms.ChoiceField(
        label='Marka samochodu', choices=CAR_BRANDS_CHOICES, required=False,
        widget=forms.Select(
            choices=CAR_BRANDS_CHOICES,
            attrs={'class': 'form-control'}
        )
    )
    category = forms.ChoiceField(
        label='Kategoria', choices=CATEGORIES_CHOICES, required=False,
        widget=forms.Select(
            choices=CATEGORIES_CHOICES,
            attrs={'class': 'form-control'}
        )
    )
    manufacturer_reference_number = forms.CharField(
        label='Numer referencyjny producenta', max_length=50, required=False,
        widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Numer referencyjny producenta'})
    )
    price_from = forms.FloatField(
        label='Cena od', required=False,
        widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Cena od'})
    )
    price_to = forms.FloatField(
        label='Cena do', required=False,
        widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Cena do'})
    )
    new = forms.BooleanField(
        label='Tylko nowe', required=False,
        widget=forms.Select(
            choices=((None, 'Tylko nowe'), (True, 'Tak'), (False, 'Nie')),
            attrs={'class': 'form-control'})
    )


class PostCreateForm(forms.ModelForm):
    """Post creation form."""

    car_brand = forms.ChoiceField(
        label='Marka samochodu', choices=CAR_BRANDS_CHOICES, required=False,
        widget=forms.Select(
            choices=CAR_BRANDS_CHOICES,
            attrs={'class': 'form-control'}
        )
    )
    category = forms.ChoiceField(
        label='Kategoria', choices=CATEGORIES_CHOICES, required=False,
        widget=forms.Select(
            choices=CATEGORIES_CHOICES,
            attrs={'class': 'form-control'}
        )
    )
    class Meta:
        model = Post
        fields = (
            'title', 'description', 'price',
            'is_new', 'manufacturer_reference_number', 'image'
        )
        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'description': forms.Textarea(attrs={'class': 'form-control'}),
            'price': forms.NumberInput(attrs={'class': 'form-control'}),
            'is_new': forms.Select(
                choices=((None, 'Nowa część'), (True, 'Tak'), (False, 'Nie')),
                attrs={'class': 'form-control'}
            ),
            'manufacturer_reference_number': forms.TextInput(
                attrs={'class': 'form-control'}
            ),
            'image': forms.ClearableFileInput(attrs={'class': 'form-control'})

        }
        labels = {
            'title': 'Tytuł',
            'description': 'Opis',
            'price': 'Cena',
            'is_new': 'Nowa część',
            'manufacturer_reference_number': 'Numer referencyjny producenta',
            'image': 'Zdjęcie'
        }


class PostBuyForm(forms.Form):
    """Post buying form."""
    post_id = forms.IntegerField(widget=forms.HiddenInput())
    delivery_type = forms.ChoiceField(
        label='Typ dostawy', choices=DELIVERY_TYPES,
        widget=forms.Select(
            choices=CATEGORIES_CHOICES,
            attrs={'class': 'form-control'}
        )
    )

    def __init__(self, post_id, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['post_id'].initial = post_id
