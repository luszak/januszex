from ast import literal_eval
from datetime import datetime, timedelta

from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.http import Http404
from django.shortcuts import (
    render, redirect, reverse
)
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_exempt
from django.views import View

from shop.forms import (
    UserCreationForm, PostFindingForm, UserForm, AddressForm, PostCreateForm,
    PostBuyForm
)
from shop.models import User, Post, Address, Purchase, City


def get_newest_posts():
    return Post.objects.order_by('-created_at').filter(
        bought=False, expiration_date__gte=datetime.now()
    )[:9]

@never_cache
def signup(request):
    """Signing up."""
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = User.objects.create_user(**form.cleaned_data)
            login(request, user)
            search_form = PostFindingForm()
            newest_posts = get_newest_posts()
            return redirect(
                '/', context={
                    'search_form': search_form,
                    'new_posts': newest_posts
                }
            )
    else:
        form = UserCreationForm()
    return render(request, 'registration/signup.html', {'form': form})


@method_decorator(never_cache, name='dispatch')
class IndexView(View):
    """Index view."""
    template_name = 'index.html'

    def get(self, request):
        """Return index page."""
        search_form = PostFindingForm()
        newest_posts = get_newest_posts()
        return render(
            request, self.template_name,
            {
                'search_form': search_form,
                'new_posts': newest_posts
            }
        )


@method_decorator(login_required, name='dispatch')
@method_decorator(never_cache, name='dispatch')
class UserDetailView(View):
    """User detail view"""
    user_form_class = UserForm
    address_form_class = AddressForm
    template_name = 'user/user.html'

    def get(self, request):
        """Return user details."""
        user = User.objects.get(pk=request.user.pk)

        user_form = self.user_form_class(initial={
            'first_name': user.first_name,
            'last_name': user.last_name,
            'email': user.email,
            'phone_number': user.phone_number
        })
        if user.address:
            address_form = self.address_form_class(initial={
                'voivodeship': user.address.city.voivodeship.name,
                'city': user.address.city.name,
                'zip_code': user.address.zip_code,
                'street': user.address.street,
                'building_number': user.address.building_number,
                'apartment_number': user.address.apartment_number
            })
        else:
            address_form = self.address_form_class()

        return render(request, self.template_name, {
            'user_form': user_form,
            'address_form': address_form
        })

    def post(self, request):
        user = User.objects.get(pk=request.user.pk)
        user_form = self.user_form_class(request.POST)
        address_form = self.address_form_class(request.POST)

        if user_form.is_valid() and address_form.is_valid():
            try:
                city = City.objects.get(
                    name=address_form.cleaned_data['city'],
                    voivodeship_id=address_form.cleaned_data['voivodeship']
                )
            except City.DoesNotExist:
                city = City.objects.create(
                    name=address_form.cleaned_data['city'],
                    voivodeship_id=address_form.cleaned_data['voivodeship']
                )
            try:
                address = Address.objects.get(
                    city=city,
                    zip_code=address_form.cleaned_data['zip_code'],
                    street=address_form.cleaned_data['street'],
                    building_number=address_form.cleaned_data['building_number'],
                    apartment_number=address_form.cleaned_data['apartment_number'],
                )
            except Address.DoesNotExist:
                address = Address.objects.create(
                    city=city,
                    zip_code=address_form.cleaned_data['zip_code'],
                    street=address_form.cleaned_data['street'],
                    building_number=address_form.cleaned_data['building_number'],
                    apartment_number=address_form.cleaned_data['apartment_number'],
                )
            user.address = address
            user.first_name = user_form.cleaned_data['first_name']
            user.last_name = user_form.cleaned_data['last_name']
            user.phone_number = user_form.cleaned_data['phone_number']
            user.save()
            return render(request, self.template_name, {
                'user_form': user_form,
                'address_form': address_form
            })
        else:
            return render(request, self.template_name, {
                'user_form': user_form,
                'address_form': address_form
            })


class UserDeleteView(View):
    """Delete user view."""
    def post(self, request):
        """Delete a user."""
        try:
            user = User.objects.get(pk=int(request.POST.get('user_id')))
            if user.id == request.user.id:
                user.delete()
        except User.DoesNotExist():
            pass
        search_form = PostFindingForm()
        newest_posts = get_newest_posts()
        return redirect(
            '/', context={
                'search_form': search_form,
                'new_posts': newest_posts
            }
        )


@method_decorator(login_required, name='dispatch')
@method_decorator(never_cache, name='dispatch')
class PostCreateView(View):
    """Post creation view."""
    form = PostCreateForm
    template_name = 'post/create.html'

    def get(self, request):
        """Return post creation form"""
        form = self.form()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        """Create a post."""
        form = self.form(request.POST, request.FILES)
        if form.is_valid():
            Post.objects.create(
                brand_id=form.cleaned_data['car_brand'],
                category_id=form.cleaned_data['category'],
                seller_id=request.user.id,
                title=form.cleaned_data['title'],
                description=form.cleaned_data['description'],
                price=form.cleaned_data['price'],
                expiration_date=datetime.now()+timedelta(weeks=2),
                is_new=form.cleaned_data['is_new'],
                manufacturer_reference_number=form.cleaned_data.get(
                    'manufacturer_reference_number', ''
                ),
                image=form.cleaned_data.get('image')
            )
            search_form = PostFindingForm()
            newest_posts = get_newest_posts()
            return redirect(
                '/', context={
                    'search_form': search_form,
                    'new_posts': newest_posts
                }
            )
        else:
            return render(request, self.template_name, {'form': form})


@method_decorator(never_cache, name='dispatch')
class PostDetailView(View):
    """Post detail view"""
    template_name = 'post/detail.html'
    post_buy_from = PostBuyForm

    def get(self, request, pk):
        """Return post details."""
        try:
            editable = False
            post = Post.objects.get(pk=pk)
        except Post.DoesNotExist:
            raise Http404()
        try:
            if User.objects.get(pk=request.user.pk) != post.seller:
                post.views_counter += 1
                post.save()
            else :
                editable = True
        except User.DoesNotExist:
            post.views_counter += 1
            post.save()
        form = self.post_buy_from(post.id)
        return render(request, self.template_name, {
            'post': post, 'seller': post.seller,
            'editable': editable, 'form': form
        })

    def post(self, request, pk):
        """Delete a post."""
        try:
            post = Post.objects.get(pk=pk)
        except Post.DoesNotExist:
            raise Http404()
        if User.objects.get(pk=request.user.pk) == post.seller:
            Post.objects.filter(pk=pk).delete()
        search_form = PostFindingForm()
        newest_posts = get_newest_posts()
        return redirect('/user/posts')


@method_decorator(login_required, name='dispatch')
@method_decorator(never_cache, name='dispatch')
class PostEditView(View):
    """Post editable details view"""
    form = PostCreateForm
    template_name = 'post/edit.html'

    def get(self, request, pk):
        """Return editable post details."""
        try:
            post = Post.objects.get(pk=pk)
        except Post.DoesNotExist:
            raise Http404()
        form = self.form()
        form = PostCreateForm(initial={
        'title': post.title,
        'description': post.description,
        'price': post.price,
        'is_new': post.is_new,
        'manufacturer_reference_number': post.manufacturer_reference_number,
        })
        return render(request, self.template_name, {'form': form, 'car_brand': post.brand.id, 'category': post.category.id, 'post_id': post.id})

    def post(self, request, pk):
        """Edit a post."""
        form = self.form(request.POST, request.FILES)
        if form.is_valid():
            post = Post.objects.get(pk=pk)
            post.brand_id=form.cleaned_data['car_brand']
            post.category_id=form.cleaned_data['category']
            post.seller_id=request.user.id
            post.title=form.cleaned_data['title']
            post.description=form.cleaned_data['description']
            post.price=form.cleaned_data['price']
            post.is_new=form.cleaned_data['is_new']
            post.manufacturer_reference_number=form.cleaned_data.get(
                'manufacturer_reference_number', ''
            )
            post.image=form.cleaned_data.get('image')
            post.save()

            search_form = PostFindingForm()
            newest_posts = get_newest_posts()
            return redirect(
                '/', context={
                    'search_form': search_form,
                    'new_posts': newest_posts
                }
            )
        else:
            return render(request, self.template_name, {'form': form})


@method_decorator(login_required, name='dispatch')
@method_decorator(never_cache, name='dispatch')
class UserPostsView(View):
    """user posts view"""
    template_name = 'user/posts.html'

    def get(self, request):
        """Return users posts."""
        try:
            posts = Post.objects.filter(seller_id = request.user.id)
        except Post.DoesNotExist:
            raise Http404()
        return render(request, self.template_name, {
            'posts': posts
        })


@method_decorator(login_required, name='dispatch')
@method_decorator(never_cache, name='dispatch')
class UserPurchasesView(View):
    """User purchases view"""
    template_name = 'user/purchases.html'

    def get(self, request):
        """Return users purchases."""
        purchases = Purchase.objects.filter(
            buyer_id = request.user.id).order_by('-date')
        return render(request, self.template_name, {
            'purchases': purchases
        })


@method_decorator(csrf_exempt, name='dispatch')
@method_decorator(never_cache, name='dispatch')
class PostSearchView(View):
    """Post search view"""
    template_name = 'post/search.html'
    form = PostFindingForm

    def get(self, request, page):
        """Return searched posts."""
        form = self.form(request.GET)
        form.is_valid()
        filters = {}
        if form.cleaned_data.get('title') != '':
            filters['title__contains'] = form.cleaned_data.get('title')
        if form.cleaned_data.get('price_from') is not None:
            filters['price__gte'] = form.cleaned_data.get('price_from')
        if form.cleaned_data.get('price_to') is not None:
            filters['price__lt'] = form.cleaned_data.get('price_to')
        if (form.cleaned_data.get('category', '') != ''
            and form.cleaned_data.get('category', '') != 0):
            filters['category_id'] = form.cleaned_data.get('category')
        if (form.cleaned_data.get('car_brand', '') != ''
            and form.cleaned_data.get('car_brand', '') != 0):
            filters['brand_id'] = form.cleaned_data.get('car_brand')
        if form.cleaned_data.get('manufacturer_reference_number', '') != '':
            filters['manufacturer_reference_number'] = form.cleaned_data.get(
                'manufacturer_reference_number')
        if form.data.get('new') is not None:
            filters['is_new'] = literal_eval(request.GET.get('new'))

        filters['expiration_date__gte'] = datetime.now()
        filters['bought'] = False

        posts = Post.objects.filter(**filters)
        paginator = Paginator(posts, 9)

        posts = paginator.get_page(page)
        return render(request, self.template_name, {
            'posts': posts, 'search_form': form, 'filters': filters
        })


@method_decorator(login_required, name='dispatch')
@method_decorator(never_cache, name='dispatch')
class PostBuyView(View):
    """Post buy view."""
    template_name = 'post/bought.html'

    def post(self, request):
        """Save post as bought by a user."""
        post_id = int(request.POST.get('post_id'))
        try:
            post = Post.objects.get(pk=post_id)
        except Post.DoesNotExist:
            search_form = PostFindingForm()
            newest_posts = get_newest_posts()
            return redirect(
                '/', context={
                    'search_form': search_form,
                    'new_posts': newest_posts
                }
            )
        post.bought = True
        post.save()
        user = User.objects.get(id=request.user.id)
        date = datetime.now()
        delivery_type_id = int(request.POST.get('delivery_type'))
        Purchase.objects.create(
            post=post,
            buyer=user,
            date=date,
            delivery_type_id=delivery_type_id
        )
        purchases = Purchase.objects.filter(
            buyer_id = request.user.id).order_by('-date')
        return render(request, 'user/purchases.html', {
            'purchases': purchases
        })
