from datetime import timedelta

from django.core.validators import RegexValidator
from django.db import models
from django.contrib.auth.models import User as AuthUser


class User(AuthUser):
    """
    Application user database object.

    Extends auth.User with phone number and relations.
    """
    phone_regex = RegexValidator(
        regex=r'^\+?1?\d{9,15}$',
        message="Phone number must be entered in the format: \
        +999999999'. Up to 15 digits allowed."
    )
    phone_number = models.CharField(validators=[phone_regex], max_length=17, blank=True)
    address = models.ForeignKey(
        'Address', null=True, on_delete=models.SET_NULL
    )


class Voivodeship(models.Model):
    """Voivodeship database object."""
    name = models.CharField(max_length=20, unique=True)

    def __str__(self):
        return self.name


class City(models.Model):
    """City database object."""
    name = models.CharField(max_length=20)
    voivodeship = models.ForeignKey(
        'Voivodeship',
        on_delete=models.PROTECT
    )

    def __str__(self):
        return "{} ({})".format(self.name, self.voivodeship.name)


class Address(models.Model):
    """Address database object."""
    city = models.ForeignKey(
        'City', on_delete=models.PROTECT
    )
    zip_code = models.CharField(max_length=6)
    street = models.CharField(max_length=50, blank=True)
    building_number = models.CharField(max_length=8)
    apartment_number = models.CharField(max_length=8, blank=True)

    def __str__(self):
        return "{} {}, {} {} {}".format(
            self.zip_code, self.city, self.street,
            self.building_number, self.apartment_number
        )

class CarBrand(models.Model):
    """Car brand database object."""
    name = models.CharField(max_length=50, unique=True)


class Category(models.Model):
    """Category database object."""
    name = models.CharField(max_length=50, unique=True)


class DeliveryType(models.Model):
    """Delivery type database object"""
    name = models.CharField(max_length=40, unique=True)
    estimated_waiting_time = models.IntegerField()  # in days
    delivery_price = models.FloatField(null=True)


def post_directory_path(instance, filename):
    """Custom path for images."""
    return 'post_{0}/{1}'.format(instance.seller.id, filename)


class Post(models.Model):
    """Post database object."""
    brand = models.ForeignKey(
        'CarBrand', on_delete=models.PROTECT
    )
    category = models.ForeignKey(
        'Category', on_delete=models.PROTECT
    )
    seller = models.ForeignKey(
        'User', null=True, on_delete=models.SET_NULL
    )
    title = models.CharField(max_length=100)
    description = models.TextField()
    price = models.FloatField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    expiration_date = models.DateTimeField()
    is_new = models.BooleanField()
    manufacturer_reference_number = models.CharField(
        max_length=50, blank=True
    )
    views_counter = models.IntegerField(default=0)
    bought = models.BooleanField(default=False)
    image = models.ImageField(blank=True, upload_to=post_directory_path)

    class Meta:
        ordering = ['-created_at']

    def __str__(self):
        return "{} ({})".format(self.title, self.seller)


class Purchase(models.Model):
    """Previous purchases database object."""
    post = models.OneToOneField('Post', on_delete=models.PROTECT)
    buyer = models.ForeignKey('User', null=True, on_delete=models.SET_NULL)
    date = models.DateTimeField('Date')
    delivery_type = models.ForeignKey(
        'DeliveryType', on_delete=models.PROTECT,
    )

    def __str__(self):
        return "{} ({})".format(self.post, self.buyer)
