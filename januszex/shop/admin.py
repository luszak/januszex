from django.contrib import admin
from shop.models import User, City, Address, Post, Purchase


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    exclude = ('password',)

@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    pass

@admin.register(Address)
class AddressAdmin(admin.ModelAdmin):
    pass

@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    pass

@admin.register(Purchase)
class PurchaseAdmin(admin.ModelAdmin):
    pass
