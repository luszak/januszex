"""januszex URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path
from django.conf.urls.static import static

from januszex import settings
from shop import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('login', auth_views.LoginView.as_view(), name='login'),
    path('logout', auth_views.LogoutView.as_view(), name='logout'),
    path('signup', views.signup, name='signup'),
    path('user', views.UserDetailView.as_view(), name='user'),
    path('user/posts', views.UserPostsView.as_view(), name='user-posts'),
    path('user/purchases', views.UserPurchasesView.as_view(), name='user-purchases'),
    path('user/delete', views.UserDeleteView.as_view(), name='user-delete'),
    path('post/create', views.PostCreateView.as_view(), name='post-create'),
    path('post/<int:pk>', views.PostDetailView.as_view(), name='post-detail'),
    path('post/edit/<int:pk>', views.PostEditView.as_view(), name='post-edit'),
    path('post/search/<int:page>', views.PostSearchView.as_view(), name='post-search'),
    path('post/buy', views.PostBuyView.as_view(), name='post-buy'),
    path('admin/', admin.site.urls),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
