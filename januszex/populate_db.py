import os

VOIVODESHIPS = [
    'dolnośląskie',
    'kujawsko-pomorskie',
    'lubelskie',
    'lubuskie',
    'łódzkie',
    'małopolskie',
    'mazowieckie',
    'opolskie',
    'podkarpackie',
    'podlaskie',
    'pomorskie',
    'śląskie',
    'świętokrzyskie',
    'warmińsko-mazurskie',
    'wielkopolskie',
    'zachodniopomorskie',
]

BRANDS = [
    'Acura',
    'Aixa',
    'Alfa Romeo',
    'Asia',
    'Aston Martin',
    'Audi',
    'Autobianchi',
    'Bentley',
    'BMW',
    'Cadillac',
    'Chatenet',
    'Chevrolet',
    'Chrysler',
    'Citroën',
    'Dacia',
    'Daewoo',
    'Daihatsu',
    'DKW',
    'Dodge',
    'Ferrari',
    'Fiat',
    'Ford',
    'Galloper',
    'Gaz',
    'Holden',
    'Honda',
    'Hyundai',
    'Infiniti',
    'Isuzu',
    'Iveco',
    'Jaguar',
    'Jeep',
    'Kia',
    'Lada',
    'Lamborghini',
    'Lancia',
    'Land Rover',
    'Lexus',
    'Ligier',
    'Lincoln',
    'Maserati',
    'Mazda',
    'McLaren',
    'Mercedes-Benz',
    'Mercury',
    'MG',
    'Microcar',
    'Mini',
    'Mitsubishi',
    'Nissan',
    'Oltcit',
    'Opel',
    'Peugeot',
    'Piaggio',
    'Polonez',
    'Pontiac',
    'Porsche',
    'Proton',
    'Renault',
    'Rover',
    'Saab',
    'Scion',
    'Seat',
    'Shuanghuan',
    'Škoda',
    'Smart',
    'SsangYong',
    'Subaru',
    'Suzuki',
    'Tata',
    'Tatra',
    'Tavria',
    'Tesla',
    'Toyota',
    'Trabant',
    'Triumph',
    'TVR',
    'Volkswagen',
    'Volvo',
    'Wartburg',
    'Zastava',
    'Żuk',
]

CATEGORIES =[
    'Akcesoria',
    'Sprzęt car audio',
    'Filtry',
    'Karoseria',
    'Elementy wnętrza',
    'Koła',
    'Felgi',
    'Opony',
    'Ogrzewanie/wentylacja/klimatyzacja',
    'Oświetlenie',
    'Skrzynia biegów',
    'Silnik',
    'Tuning',
    'Układ elektryczny',
    'Układ hamulcowy',
    'Układ kierowniczy',
    'Układ napędowy',
    'Układ paliwowy',
    'Układ wydechowy',
    'Układ zawieszenia',
    'Inne',
]

DELIVERY_TYPES = [
    ('Przesyłka kurierska', 3, 15),
    ('Paczkomat', 3, 10),
]

def populate():
    print("\n Voivodeships")
    for name in VOIVODESHIPS:
        print('.', end='')
        add_voivodeship(name)

    print("\n Brands")
    for name in BRANDS:
        print('.', end='')
        add_car_brand(name)

    print("\n Categories")
    for name in CATEGORIES:
        print('.', end='')
        add_category(name)

    print("\n Delivery types")
    for name, time, price in DELIVERY_TYPES:
        print('.', end='')
        add_delivery_type(name, time, price)

def add_voivodeship(name):
    v = Voivodeship.objects.create(name=name)
    return v


def add_car_brand(name):
    c = CarBrand.objects.create(name=name)
    return c


def add_category(name):
    c = Category.objects.create(name=name)
    return c


def add_delivery_type(name, time, price):
    d = DeliveryType.objects.create(
        name=name, estimated_waiting_time=time, delivery_price=price
    )
    return d


if __name__ == '__main__':
    print ("Starting population script...")
    import django
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'januszex.settings')
    django.setup()
    from shop.models import Voivodeship, CarBrand, Category, DeliveryType
    print("Adding...")
    populate()
    print('\n Done!')
