# Instalacja PostgreSQL

```
sudo apt-get install python3-pip python3-dev libpq-dev postgresql postgresql-contrib
```

# Tworzenie bazy
```
sudo su - postgres
psql
```

```
CREATE USER devel WITH PASSWORD 'devel';
CREATE DATABASE januszex OWNER devel;
\q
exit
```

# Instalowanie zależności
```
pip install -r requirements.txt
```

# Aplikowanie migracji
```
./manage.py migrate
```
# Populacja bazy
```
python populate_db.py
```

# Uruchomienie serwera developerskiego
```
./manage.py runserver
```
